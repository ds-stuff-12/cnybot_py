import http.client

conn = http.client.HTTPSConnection("weatherapi-com.p.rapidapi.com")

headers = {
    'x-rapidapi-host': "weatherapi-com.p.rapidapi.com",
    'x-rapidapi-key': "use your own key"
    }

conn.request("GET", "/current.json?q=13212", headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))